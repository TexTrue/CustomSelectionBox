package tennox.customselectionbox;

import net.minecraft.client.gui.widget.AbstractSlider;

import static tennox.customselectionbox.CSBConfig.*;

public class CSBSlider extends AbstractSlider {
    
    private int id;
    
    // GuiOptionSlider
    public CSBSlider(int i, int x, int y, float f) {
        super(x, y, 150, 20, f);
        this.id = i;
        applyValue();
        updateMessage();
    }
    
    private void updateValue(int id, float sliderValue) {
        switch (id) {
            case 1:
                setRed(sliderValue);
                break;
            case 2:
                setGreen(sliderValue);
                break;
            case 3:
                setBlue(sliderValue);
                break;
            case 4:
                setAlpha(sliderValue);
                break;
            case 5:
                setThickness(sliderValue * 7.0F);
                break;
            case 7:
                setBlinkAlpha(sliderValue);
                break;
            case 8:
                setBlinkSpeed(sliderValue);
        }
    }
    
    private String getDisplayString(int id, float sliderValue) {
        switch (id) {
            case 1:
                return "Red: " + Math.round(sliderValue * 255.0F);
            case 2:
                return "Green: " + Math.round(sliderValue * 255.0F);
            case 3:
                return "Blue: " + Math.round(sliderValue * 255.0F);
            case 4:
                return "Alpha: " + Math.round(sliderValue * 255.0F);
            case 5:
                return "Thickness: " + Math.round(sliderValue * 7.0F);
            case 7:
                return "Blink Alpha: " + Math.round(sliderValue * 255.0F);
            case 8:
                return "Blink Speed: " + Math.round(sliderValue * 100.0F);
        }
        return "Option Error?! (" + id + ")";
    }
    
    @Override
    protected void updateMessage() {
        setMessage(getDisplayString(id, (float) value));
    }
    
    @Override
    protected void applyValue() {
        updateValue(id, (float) value);
    }
}
