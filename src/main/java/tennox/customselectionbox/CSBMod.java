package tennox.customselectionbox;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.block.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.OptionsScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.DestroyBlockProgress;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.state.properties.BedPart;
import net.minecraft.state.properties.DoubleBlockHalf;
import net.minecraft.state.properties.PistonType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.World;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static tennox.customselectionbox.CSBConfig.*;

@Mod(value = "csb")
public class CSBMod {
    
    private static final Logger LOGGER = LogManager.getLogger();
    
    public CSBMod() {
        CSBConfig.onInit();
        MinecraftForge.EVENT_BUS.register(this);
    }
    
    public static void drawNewOutlinedBoundingBox(VoxelShape voxelShapeIn, double xIn, double yIn, double zIn, float red, float green, float blue, float alpha) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(1, DefaultVertexFormats.POSITION_COLOR);
        voxelShapeIn.forEachEdge((p_195468_11_, p_195468_13_, p_195468_15_, p_195468_17_, p_195468_19_, p_195468_21_) -> {
            bufferbuilder.pos(p_195468_11_ + xIn, p_195468_13_ + yIn, p_195468_15_ + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(p_195468_17_ + xIn, p_195468_19_ + yIn, p_195468_21_ + zIn).color(red, green, blue, alpha).endVertex();
        });
        tessellator.draw();
    }
    
    public static void drawNewBlinkingBlock(VoxelShape voxelShapeIn, double xIn, double yIn, double zIn, float red, float green, float blue, float alpha) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        voxelShapeIn.forEachBox((x1, y1, z1, x2, y2, z2) -> {
            //Up
            bufferbuilder.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
            bufferbuilder.pos(x1 + xIn, y1 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y1 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y1 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y1 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y1 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            tessellator.draw();
            
            //Down
            bufferbuilder.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
            bufferbuilder.pos(x1 + xIn, y2 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y2 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y2 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y2 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y2 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            tessellator.draw();
            
            //North
            bufferbuilder.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
            bufferbuilder.pos(x1 + xIn, y1 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y2 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y2 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y1 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y1 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            tessellator.draw();
            
            //South
            bufferbuilder.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
            bufferbuilder.pos(x1 + xIn, y1 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y1 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y2 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y2 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y1 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            tessellator.draw();
            
            //West
            bufferbuilder.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
            bufferbuilder.pos(x1 + xIn, y1 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y1 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y2 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y2 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x1 + xIn, y1 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            tessellator.draw();
            
            //East
            bufferbuilder.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
            bufferbuilder.pos(x2 + xIn, y1 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y2 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y2 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y1 + yIn, z2 + zIn).color(red, green, blue, alpha).endVertex();
            bufferbuilder.pos(x2 + xIn, y1 + yIn, z1 + zIn).color(red, green, blue, alpha).endVertex();
            tessellator.draw();
        });
    }
    
    private static float getBlockDamage(BlockPos pos) {
        try {
            Field f;
            try {
                f = WorldRenderer.class.getDeclaredField("damagedBlocks");
            } catch (NoSuchFieldException e) {
                f = WorldRenderer.class.getDeclaredField("field_72738_E");
            }
            f.setAccessible(true);
            HashMap<Integer, DestroyBlockProgress> map = (HashMap<Integer, DestroyBlockProgress>) f.get(Minecraft.getInstance().worldRenderer);
            for(Map.Entry<Integer, DestroyBlockProgress> entry : map.entrySet()) {
                DestroyBlockProgress prg = entry.getValue();
                if (prg.getPosition().equals(pos)) {
                    if (prg.getPartialBlockDamage() >= 0 && prg.getPartialBlockDamage() <= 10)
                        return prg.getPartialBlockDamage() / 10f;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0f;
    }
    
    public static void openSettingsGUI() {
        Minecraft mc = Minecraft.getInstance();
        mc.gameSettings.saveOptions();
        mc.displayGuiScreen(new CSBSettingsGui(null));
    }
    
    @SubscribeEvent
    public void initScreen(GuiScreenEvent.InitGuiEvent event) {
        if (event.getGui() instanceof OptionsScreen) {
            Screen gui = event.getGui();
            CSBButton button = null;
            if (diffButtonLoc)
                button = new CSBButton(gui.width - 150, 0, 150, 20, "Custom Selection Box");
            else
                button = new CSBButton(gui.width / 2 - 75, gui.height / 6 + 24 - 6, 150, 20, "Custom Selection Box");
            event.addWidget(button);
        }
    }
    
    @SubscribeEvent
    public void onRenderSelectionBox(DrawBlockHighlightEvent event) {
        if (event.getSubID() != 0 || event.getTarget().getType() != RayTraceResult.Type.BLOCK)
            return;
        event.setCanceled(true);
        BlockPos pos = ((BlockRayTraceResult) event.getTarget()).getPos();
        World world = event.getInfo().func_216773_g().world;
        BlockState blockstate = world.getBlockState(pos);
        if (!blockstate.isAir(world, pos) && world.getWorldBorder().contains(pos)) {
            GlStateManager.enableBlend();
            GlStateManager.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
            GlStateManager.lineWidth(getThickness());
            GlStateManager.disableTexture();
            GlStateManager.depthMask(false);
            GlStateManager.matrixMode(5889);
            GlStateManager.pushMatrix();
            GlStateManager.scalef(1.0F, 1.0F, 0.999F);
            float red = getRed(), green = getGreen(), blue = getBlue(), alpha = getAlpha();
            if (rainbow) {
                final Color color = Color.getHSBColor((float) (System.currentTimeMillis() % 10000L / 10000.0f), 0.8f, 0.8f);
                red = color.getRed() / 255.0f;
                green = color.getGreen() / 255.0f;
                blue = color.getBlue() / 255.0f;
            }
            VoxelShape shape = blockstate.getShape(world, pos, ISelectionContext.forEntity(event.getInfo().func_216773_g()));
            if (adjustBoundingBoxByLinkedBlocks)
                shape = csb_adjustShapeByLinkedBlocks(world, world.getBlockState(pos), pos, shape);
            double d0 = event.getInfo().func_216785_c().x;
            double d1 = event.getInfo().func_216785_c().y;
            double d2 = event.getInfo().func_216785_c().z;
            final float blinkingAlpha = (getBlinkSpeed() > 0 && !breakAnimation.equals(BreakAnimationType.ALPHA)) ? getBlinkAlpha() * (float) Math.abs(Math.sin(System.currentTimeMillis() / 100.0D * getBlinkSpeed())) : getBlockDamage(pos);
            drawNewBlinkingBlock(shape, pos.getX() - d0, pos.getY() - d1, pos.getZ() - d2, red, green, blue, blinkingAlpha);
            drawNewOutlinedBoundingBox(shape, pos.getX() - d0, pos.getY() - d1, pos.getZ() - d2, red, green, blue, alpha);
            GlStateManager.popMatrix();
            GlStateManager.matrixMode(5888);
            GlStateManager.depthMask(true);
            GlStateManager.enableTexture();
            GlStateManager.disableBlend();
        }
    }
    
    private VoxelShape csb_adjustShapeByLinkedBlocks(World world, BlockState blockState, BlockPos blockPos, VoxelShape shape) {
        try {
            if (blockState.getBlock() instanceof ChestBlock) {
                // Chests
                Block block = blockState.getBlock();
                Direction direction = ChestBlock.getDirectionToAttached(blockState);
                BlockState anotherChestState = world.getBlockState(blockPos.offset(direction, 1));
                if (anotherChestState.getBlock() == block)
                    if (blockPos.offset(direction, 1).offset(ChestBlock.getDirectionToAttached(anotherChestState)).equals(blockPos))
                        return VoxelShapes.or(shape, anotherChestState.getShape(world, blockPos).withOffset(direction.getXOffset(), direction.getYOffset(), direction.getZOffset()));
            } else if (blockState.getBlock() instanceof DoorBlock) {
                // Doors
                Block block = blockState.getBlock();
                if (blockState.get(DoorBlock.HALF).equals(DoubleBlockHalf.LOWER) && world.getBlockState(blockPos.up(1)).getBlock() == block) {
                    BlockState otherState = world.getBlockState(blockPos.up(1));
                    if (otherState.get(DoorBlock.POWERED).equals(blockState.get(DoorBlock.POWERED)) && otherState.get(DoorBlock.FACING).equals(blockState.get(DoorBlock.FACING)) && otherState.get(DoorBlock.HINGE).equals(blockState.get(DoorBlock.HINGE))) {
                        return VoxelShapes.or(shape, otherState.getShape(world, blockPos).withOffset(0, 1, 0));
                    }
                }
                if (blockState.get(DoorBlock.HALF).equals(DoubleBlockHalf.UPPER) && world.getBlockState(blockPos.down(1)).getBlock() == block) {
                    BlockState otherState = world.getBlockState(blockPos.down(1));
                    if (otherState.get(DoorBlock.POWERED).equals(blockState.get(DoorBlock.POWERED)) && otherState.get(DoorBlock.FACING).equals(blockState.get(DoorBlock.FACING)) && otherState.get(DoorBlock.HINGE).equals(blockState.get(DoorBlock.HINGE)))
                        return VoxelShapes.or(shape, otherState.getShape(world, blockPos).withOffset(0, -1, 0));
                }
            } else if (blockState.getBlock() instanceof BedBlock) {
                // Beds
                Block block = blockState.getBlock();
                Direction direction = blockState.get(HorizontalBlock.HORIZONTAL_FACING);
                BlockState otherState = world.getBlockState(blockPos.offset(direction));
                if (blockState.get(BedBlock.PART).equals(BedPart.FOOT) && otherState.getBlock() == block) {
                    if (otherState.get(BedBlock.PART).equals(BedPart.HEAD))
                        return VoxelShapes.or(shape, otherState.getShape(world, blockPos).withOffset(direction.getXOffset(), direction.getYOffset(), direction.getZOffset()));
                }
                otherState = world.getBlockState(blockPos.offset(direction.getOpposite()));
                if (blockState.get(BedBlock.PART).equals(BedPart.HEAD) && otherState.getBlock() == block) {
                    if (otherState.get(BedBlock.PART).equals(BedPart.FOOT))
                        return VoxelShapes.or(shape, otherState.getShape(world, blockPos).withOffset(direction.getXOffset(), direction.getYOffset(), direction.getZOffset()));
                }
            } else if (blockState.getBlock() instanceof PistonBlock && blockState.get(PistonBlock.EXTENDED)) {
                // Piston Base
                Block block = blockState.getBlock();
                Direction direction = blockState.get(DirectionalBlock.FACING);
                BlockState otherState = world.getBlockState(blockPos.offset(direction));
                if (otherState.get(PistonHeadBlock.TYPE).equals(block == Blocks.PISTON ? PistonType.DEFAULT : PistonType.STICKY) && direction.equals(otherState.get(DirectionalBlock.FACING)))
                    return VoxelShapes.or(shape, otherState.getShape(world, blockPos).withOffset(direction.getXOffset(), direction.getYOffset(), direction.getZOffset()));
            } else if (blockState.getBlock() instanceof PistonHeadBlock) {
                // Piston Arm
                Block block = blockState.getBlock();
                Direction direction = blockState.get(DirectionalBlock.FACING).getOpposite();
                BlockState otherState = world.getBlockState(blockPos.offset(direction));
                if (direction.getOpposite().equals(otherState.get(DirectionalBlock.FACING)) && otherState.get(PistonBlock.EXTENDED) && (otherState.getBlock() == (blockState.get(PistonHeadBlock.TYPE).equals(PistonType.DEFAULT) ? Blocks.PISTON : Blocks.STICKY_PISTON)))
                    return VoxelShapes.or(shape, otherState.getShape(world, blockPos).withOffset(direction.getXOffset(), direction.getYOffset(), direction.getZOffset()));
            }
        } catch (Exception e) {
        }
        return shape;
    }
    
}
