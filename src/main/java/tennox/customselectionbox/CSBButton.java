package tennox.customselectionbox;

import net.minecraft.client.gui.widget.button.Button;

public class CSBButton extends Button {
    
    public CSBButton(int p_i46323_2_, int p_i46323_3_, int p_i46323_4_, int p_i46323_5_, String p_i46323_6_) {
        super(p_i46323_2_, p_i46323_3_, p_i46323_4_, p_i46323_5_, p_i46323_6_, button -> CSBMod.openSettingsGUI());
    }
    
}
