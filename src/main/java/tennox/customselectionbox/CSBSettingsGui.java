package tennox.customselectionbox;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.AbstractButton;
import net.minecraft.util.text.StringTextComponent;

import java.io.FileNotFoundException;
import java.util.Arrays;

import static tennox.customselectionbox.CSBConfig.*;
import static tennox.customselectionbox.CSBMod.openSettingsGUI;

public class CSBSettingsGui extends Screen {
    
    private Screen parent;
    private ConfigCache configCache;
    
    public CSBSettingsGui(Screen p) {
        super(new StringTextComponent(""));
        this.parent = p;
    }
    
    @Override
    public boolean keyPressed(int int_1, int int_2, int int_3) {
        if (int_1 == 256) {
            Minecraft.getInstance().displayGuiScreen(parent);
            return true;
        }
        return super.keyPressed(int_1, int_2, int_3);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    protected void init() {
        this.configCache = new ConfigCache(enabled, red, green, blue, alpha, thickness, blinkAlpha, blinkSpeed, disableDepthBuffer, breakAnimation, rainbow, adjustBoundingBoxByLinkedBlocks);
        this.buttons.clear();
        // left
        addButton(new CSBSlider(1, 4, this.height / 2 - 62, getRed()));
        addButton(new CSBSlider(2, 4, this.height / 2 - 38, getGreen()));
        addButton(new CSBSlider(3, 4, this.height / 2 - 14, getBlue()));
        addButton(new CSBSlider(4, 4, this.height / 2 + 10, getAlpha()));
        addButton(new CSBSlider(5, 4, this.height / 2 + 34, getThickness() / 7.0F));
        
        // right
        addButton(new AbstractButton(this.width - 154, this.height / 2 - 38, 150, 20, "Break Animation: " + breakAnimation.getText()) {
            @Override
            public void onPress() {
                setBreakAnimation(breakAnimation.equals(BreakAnimationType.values()[BreakAnimationType.values().length - 1]) ? BreakAnimationType.NONE : BreakAnimationType.values()[Arrays.asList(BreakAnimationType.values()).indexOf(breakAnimation) + 1]);
                setMessage("Break Animation: " + breakAnimation.getText());
            }
        });
        addButton(new CSBSlider(7, this.width - 154, this.height / 2 - 14, getBlinkAlpha()));
        addButton(new CSBSlider(8, this.width - 154, this.height / 2 + 10, getBlinkSpeed()));
        addButton(new AbstractButton(this.width - 154, this.height / 2 - 62, 150, 20, "Chroma: " + (usingRainbow() ? "ON" : "OFF")) {
            @Override
            public void onPress() {
                setIsRainbow(!usingRainbow());
                setMessage("Chroma: " + (usingRainbow() ? "ON" : "OFF"));
            }
        });
        addButton(new AbstractButton(this.width - 154, this.height / 2 + 34, 150, 20, "Link Blocks: " + (isAdjustBoundingBoxByLinkedBlocks() ? "ON" : "OFF")) {
            @Override
            public void onPress() {
                setAdjustBoundingBoxByLinkedBlocks(!isAdjustBoundingBoxByLinkedBlocks());
                setMessage("Link Blocks: " + (isAdjustBoundingBoxByLinkedBlocks() ? "ON" : "OFF"));
            }
        });
        
        //below
        addButton(new AbstractButton(this.width / 2 - 100, this.height - 48, 95, 20, "Enabled: " + (isEnabled() ? "True" : "False")) {
            @Override
            public void onPress() {
                setEnabled(!isEnabled());
                setMessage("Enabled: " + (isEnabled() ? "True" : "False"));
            }
        });
        addButton(new AbstractButton(this.width / 2 + 5, this.height - 48, 95, 20, "Save") {
            @Override
            public void onPress() {
                try {
                    saveConfig();
                    configCache = new ConfigCache(CSBConfig.enabled, red, green, blue, alpha, thickness, blinkAlpha, blinkSpeed, disableDepthBuffer, breakAnimation, rainbow, adjustBoundingBoxByLinkedBlocks);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Minecraft.getInstance().displayGuiScreen(null);
            }
        });
        addButton(new AbstractButton(this.width / 2 - 100, this.height - 24, 95, 20, "CSB defaults") {
            @Override
            public void onPress() {
                try {
                    reset(false);
                    saveConfig();
                    configCache = new ConfigCache(CSBConfig.enabled, red, green, blue, alpha, thickness, blinkAlpha, blinkSpeed, disableDepthBuffer, breakAnimation, rainbow, adjustBoundingBoxByLinkedBlocks);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                openSettingsGUI();
            }
        });
        addButton(new AbstractButton(this.width / 2 + 5, this.height - 24, 95, 20, "MC defaults") {
            @Override
            public void onPress() {
                try {
                    reset(true);
                    saveConfig();
                    configCache = new ConfigCache(CSBConfig.enabled, red, green, blue, alpha, thickness, blinkAlpha, blinkSpeed, disableDepthBuffer, breakAnimation, rainbow, adjustBoundingBoxByLinkedBlocks);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                openSettingsGUI();
            }
        });
    }
    
    @Override
    public void render(int par1, int par2, float par3) {
        if (this.minecraft.field_71441_e == null) {
            this.fillGradient(0, 0, this.width, this.height, -1072689136, -804253680);
            net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(new net.minecraftforge.client.event.GuiScreenEvent.BackgroundDrawnEvent(this));
        }
        fillGradient(0, 0, this.width, 48 - 4, -1072689136, -804253680); // top
        fillGradient(0, this.height / 2 - 67, 158, this.height / 2 + 59, -1072689136, -804253680); // left
        fillGradient(this.width - 158, this.height / 2 - 67, this.width, this.height / 2 + 59, -1072689136, -804253680); // right
        fillGradient(0, this.height - 48 - 4, this.width, this.height, -1072689136, -804253680); // bottom
        
        drawCenteredString(this.font, "Custom Selection Box", this.width / 2, (this.height - (this.height + 4 - 48)) / 2 - 4, 16777215);
        
        super.render(par1, par2, par3);
    }
    
    @Override
    public boolean isPauseScreen() {
        return false;
    }
    
    @Override
    public void onClose() {
        configCache.save();
        this.configCache = new ConfigCache(enabled, red, green, blue, alpha, thickness, blinkAlpha, blinkSpeed, disableDepthBuffer, breakAnimation, rainbow, adjustBoundingBoxByLinkedBlocks);
    }
    
}
